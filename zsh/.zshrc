#   ____    ____
#  / ___|  / ___|
# | |  _  | |
# | |_| | | |___
#  \____|  \____|

#[ -z $DISPLAY ] && [ $XDG_VTNR -eq 1 ] && startx ~/.config/x11/xinitrc


# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
setopt prompt_subst
PROMPT="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
RPROMPT="%B%D{%H:%M:%S}"
precmd() $funcstack[1]() echo   # Print lines between commands
setopt autocd                   # Automatically cd into typed directory.
stty stop undef		            # Disable ctrl-s to freeze terminal.
setopt interactive_comments

# Report command running time if it is more than 3 seconds
REPORTTIME=3

# Keep a lot of history
HISTSIZE=10000000
SAVEHIST=10000000
HISTFILE=~/.cache/zsh/history
#setopt HIST_IGNORE_ALL_DUPS     # do not put duplicated command into history list
#setopt HIST_SAVE_NO_DUPS        # do not save duplicated command
setopt HIST_REDUCE_BLANKS       # remove unnecessary blanks
setopt INC_APPEND_HISTORY_TIME  # append command to history file immediately after execution
setopt EXTENDED_HISTORY         # record command start time

covid () {
    curl https://corona-stats.online/$1
}

news () {
    curl getnews.tech/$1
}

newscript () {
    cp ~/scripts/templete ~/scripts/$1 && vi ~/scripts/$1
}

newtex () {
	cp ~/Documents/latex/templete.tex ~/Documents/latex/$1 && vi ~/Documents/latex/$1
}

wf () {
	doas cat /etc/NetworkManager/system-connections/"$1".nmconnection
}

# Basic auto/tab complete:
autoload -U compinit
compinit
zstyle ':completion::complete:*' use-cache 1
zstyle ':completion:*' menu select
zmodload zsh/complist
_comp_options+=(globdots)		# Include hidden files.

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp" >/dev/null
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}

bindkey -s '^a' 'bc -lq\n'

bindkey -s '^f' 'cd "$(dirname "$(fzf)")"\n'

bindkey '^[[P' delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

source ~/.config/zsh/.aliases

# Load syntax highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
